$(document).ready(function() {
	var ACTIVE_VIEW = 'village';

	_buildings = [
		{
			name: 'Ferme hydroponique',
			description: 'Une petite ferme produisant des denrées basiques à partir d\'eau croupie et de graines douteuses.',
			cost: [
				{
					type: 'scraps',
					value: 5
				},
				{
					type: 'energy',
					value: 5
				}
			],
			production: [
				{
					type: 'energy',
					value: 1,
					produce: true
				}
			],
		},
		{
			name: 'Recycleur',
			description: 'Compacte automatiquement la feraille des déchets alentours, attention aux doigts.',
			cost: [
				{
					type: 'scraps',
					value: 20
				},
				{
					type: 'energy',
					value: 30
				}
			],
			production: [
				{
					type: 'scraps',
					value: 1,
					produce: true
				}
			],
			increase: 1.5,
		}
	];

	_actions = [
		{
			name: 'Fouiller dans les débris',
			description: 'Une forte odeur se dégage des débris, des rats semblent y avoir élu domicile. On devrait pouvoir y trouver quelque chose d\'utile.',
			cost: [
				{
					type: 'energy',
					value: 1
				}
			],
			production: [
				{
					type: 'scraps',
					value: 1
				}
			]
		},
		{
			name: 'Chercher un rescapé',
			description: 'Beaucoup de gens ont survécu en se barricadant dans les villes. De la main d\'oeuvre ne serait pas de refus.',
			cost: [
				{
					type: 'energy',
					value: 50
				}
			],
			production: [
				{
					type: 'humans',
					value: 1
				}
			]
		}
	];

	_defenses = [
		{
			name: 'Barrière rudimentaire',
			description: 'Une barrière faite de déchets, ça ne tiendra pas longtemps.',
			currencyConsumed: 'scraps',
			cost: [
				{
					type: 'scraps',
					value: 20
				},
				{
					type: 'energy',
					value: 50
				}
			],
			production: [
				{
					type: 'defense',
					value: 10
				}
			]
		}
	];

	_missions = [
		{
			name: 'Piller le centre commercial',
			description: 'Un centre commercial abandonné se trouve à 200 mètres au sud. On devrait y trouver pas mal de ressources utiles.',
			cost: [
				{
					type: 'energy',
					value: 100
				},
				{
					type: 'humans',
					value: 1
				}
			],
			duration: 10,
			risk: 0.2,
			production: [
				{
					type: 'scraps',
					value: 50,
					hide: true
				}
			]
		}
	];

	_documents = [
		{
			name: 'Morceau de journal',
			text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed mauris ac ipsum imperdiet fringilla. Integer eu aliquam tellus. Proin enim tortor, tincidunt sit amet tortor in, gravida laoreet neque. Ut sodales in lorem sed suscipit. Nulla in dolor eleifend, maximus velit eget, placerat turpis. Proin ipsum metus, pretium eget ultricies sed, luctus id ante. Morbi lorem sem, fermentum vel augue nec, aliquam semper eros.'
		},
		{
			name: 'Journal intime',
			text: 'Dolor sit ipsum lorem, consectetur adipiscing elit. Nullam sed mauris ac ipsum imperdiet fringilla. Integer eu aliquam tellus. Proin enim tortor, tincidunt sit amet tortor in, gravida laoreet neque. Ut sodales in lorem sed suscipit. Nulla in dolor eleifend, maximus velit eget, placerat turpis. Proin ipsum metus, pretium eget ultricies sed, luctus id ante. Morbi lorem sem, fermentum vel augue nec, aliquam semper eros.'
		}
	];

	Game.init(_buildings, _actions, _defenses, _missions, _documents);

	$('#nav-buttons li').on('click', function() {
		$('#nav-buttons li').removeClass('active');
		$(this).addClass('active');

		$('.body-bloc').hide();
		if ($(this).hasClass('village')) {
			$('#village-bloc').show();
			ACTIVE_VIEW = 'village';
		}
		else if ($(this).hasClass('tech')) {
			$('#technologies-bloc').show();
			ACTIVE_VIEW = 'technologies';
		}
		else if ($(this).hasClass('defenses')) {
			$('#defenses-bloc').show();
			ACTIVE_VIEW = 'defenses';
		}
		else if ($(this).hasClass('missions')) {
			$('#missions-bloc').show();
			ACTIVE_VIEW = 'missions';
		}
		else if ($(this).hasClass('documents')) {
			$('#documents-bloc').show();
			ACTIVE_VIEW = 'documents';
		}
	});
});
