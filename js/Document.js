var Document = function(options) {
	return $.extend({
		read: false,
		visible: true,
		menu: undefined,
		content: undefined,
		menuElement: undefined,

		check: function() {
			if (!this.read && this.visible)
				return true;
			else
				return false;
		},

		displayText: function() {
			this.content.text(this.text);
			this.read = true;

			$('#documents-menu .nav-link').removeClass('active');

			this.menuElement.addClass('active');

			Game._checkAll();
		},

		displayMenuElement: function() {
			var self = this;

			if (!this.visible)
				return false;

			this.menuElement = $('<a/>')
					.attr('href', '#')
					.addClass('nav-link')
					.text(this.name)
					.on('click', function() {
						self.displayText();
					})
					.appendTo(this.menu);

			var tmp = $('<li/>')
					.addClass('nav-item')
					.append(this.menuElement);

			this.menu.append(tmp);
		},

		init: function() {
			this.menu = $('#documents-menu');
			this.content = $('#documents-content');

			this.displayMenuElement();

			return this;
		}
	},  options);
};
