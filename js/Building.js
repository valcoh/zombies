var Building = function(options) {
	return $.extend(GameEntity(), {
		quantity: 0,
		increase: 1.1,

		produce: function() {
			var self = this;

			$.each(this.production, function(i, prod) {
				Game[prod.type] += self.quantity * prod.value;
			});
		},

		init: function() {
			$('#building-display').append(this.generateCard());

			this.check();

			return this;
		}
	}, options);
};
