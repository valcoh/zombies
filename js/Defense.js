var Defense = function(options) {
	return $.extend(GameEntity(), {
		quantity: 0,
		increase: 1.1,
		buy: function() {
			var self = this;

			$.each(this.cost, function(i, cost) {
				Game[cost.type] -= cost.value;
				cost.value = Math.ceil(cost.value * self.increase);
			});

			$.each(this.production, function(i, cost) {
				Game[prod.type] += prod.value;
			});

			this.quantity++;

			Game._checkAll();
			this.updateView();
		},

		init: function() {
			$('#defenses-display').append(this.generateCard());

			this.check();

			return this;
		}
	},  options);
};
