var Mission = function(options) {
	return $.extend(GameEntity(), {
		buy: function() {
			var self = this;

			$.each(this.cost, function(i, cost) {
				Game[cost.type] -= cost.value;
			});

			this.button.hide();

			Game._checkAll();
			this.updateView();
		},

		progressBar: function() {
			var self = this;

			this.display.find('.card-body').append('<div class="progress"><div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">'+this.duration+'s</div></div>');

			var i = 0;
			var domBar = this.display.find('.progress-bar');

			domBar.css('animation-duration', self.duration + 's');

			var progress = setInterval(function() {
				if (i == self.duration) {
					self.displayResults();
					clearInterval(progress);
				}

				domBar.text(self.duration - i + 's')
				i++;
			}, 1000);
		},

		getSurvivorsCost: function() {
			var self = this;

			var res = null;

			$.each(this.cost, function(i, cost) {
				console.log(cost);
				if (cost.type == 'humans')
					res = cost.value;
			});

			return res;
		},

		displayResults: function() {
			var self = this;

			this.display.find('.progress').remove();

			this.button.show();

			var res = Math.random();

			if (res > this.risk) {
				// Mission success
				var tmp = '';
				$.each(this.production, function(i, prod) {
					tmp += '<br><span class="icon"><i class="fa ' + Game.getResourceIcon(prod.type) + '"></i></span>+' + prod.value + ' ' + Game.getResourceName(prod.type);

					Game[prod.type] += prod.value;
				});

				Game.humans += this.getSurvivorsCost();

				this.display.find('.card-body').append('<div class="alert alert-success alert-dismissible fade show mt-3" role="alert">Mission réussie.' + tmp + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			}
			else {
				// Mission failure
				var tmp = '<br><span class="icon"><i class="fa fa-user"></i></span>-' + this.getSurvivorsCost() + ' survivants';
				this.display.find('.card-body').append('<div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">Echec de la mission.' + tmp + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			}

			this.display.find('.card-body').append();
		},

		updateView: function() {
			this.progressBar();

			Game._updateView();
		},

		init: function() {
			$('#missions-display').append(this.generateCard());

			this.check();

			return this;
		}
	},  options);
};
