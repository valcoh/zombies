var GameEntity = function() {
	return {
		button: undefined,
		display: undefined,
		tooltip: undefined,

		check: function() {
			var self = this;

			var failed = false;

			$.each(this.cost, function(i, cost) {
				if (cost.value > Game[cost.type]) {
					self.button.attr("disabled", "disabled");
					failed = true;
				}
			});

			if (!failed) {
				this.button.removeAttr("disabled");
				return true;
			}

			return false;
		},

		updateCostDifference: function() {
			var self = this;

			var res = '';

			var buttonWrap = this.button.closest('.button-wrap');

			$.each(this.cost, function(i, cost) {
				if (Game[cost.type] < cost.value) {
					res += 'Manque ' + (cost.value - Game[cost.type]) + ' ' + Game.getResourceName(cost.type) + '<br>';
				}
			});

			this.tooltip.tooltip('dispose')
					.attr("title", res.slice(0, -4))
					.tooltip();
		},

		buy: function() {
			var self = this;

			$.each(this.cost, function(i, cost) {
				Game[cost.type] -= cost.value;
				cost.value = Math.ceil(cost.value * self.increase);
			});

			this.quantity++;

			Game._checkAll();
			this.updateView();
		},

		updateView: function() {
			this.display.find('.build-cost').html(this.generateCosts());
			this.display.find('.card-title').html(this.name + '<span class="badge badge-primary ml-2">' + this.quantity + '</span>');
			Game._updateView();
		},

		generateCosts: function() {
			var self = this;

			var tmp = '';

			$.each(this.cost, function(i, cost) {
				tmp += '<span class="icon"><i class="fa ' + Game.getResourceIcon(cost.type) + '"></i></span><span class="loss-exch">-' + cost.value + ' ' + Game.getResourceName(cost.type) + '</span><br>';
			});

			return tmp;
		},

		generateCard: function() {
			var self = this;

			this.button = $("<button/>")
							.text(this.name)
							.addClass("btn btn-primary")
							.click(function() {
								self.buy();
							});

			var cost = this.generateCosts();

			var production = '';

			$.each(this.production, function(i, prod) {
				if (prod.hide)
					return true;

				var tmp = '';

				if (prod.produce)
					tmp = '/ secondes';
				else
					tmp = '';

				production += '<span class="icon"><i class="fa ' + Game.getResourceIcon(prod.type) + '"></i></span><span class="win-exch">+' + prod.value + ' ' + Game.getResourceName(prod.type) + ' ' + tmp + '</span><br>';
			});

			production = production.slice(0, -4);

			this.display = $('<div class="col-md-6"><div class="card mb-3"><div class="card-body"><h5 class="card-title">' + this.name + '</h5><p class="card-text"><i>' + this.description + '</i></p>' +
			'<p><span class="build-cost">' + cost + '</span>' + production + '</p><div class="button-wrap"></div></div></div></div>');

			this.tooltip = this.display.find('.card-body').find('.button-wrap');

			this.tooltip.append(this.button)
					.on('mouseover', function() {
						self.updateCostDifference();
						return true;
					})
					.attr("data-toggle", "tooltip")
					.attr("data-placement", "right")
					.attr("data-html", "true")
					.attr("title", "")
					.tooltip();

			return this.display;
		},

		init: function() {
			$('display').append(this.generateCard());

			this.check();

			return this;
		}
	};
};
