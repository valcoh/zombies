var Game = {
	// Core variables
	handle: undefined,
	interval: 1000, // in milliseconds

	// Game variables
	humans: 2,
	scraps: 0,
	energy: 20,
	defense: 10,
	buildings: [],
	actions: [],
	defenses: [],
	missions: [],
	documents: [],
	attack: undefined,

	// Notifications
	villageNotification: 0,
	technologiesNotification: 0,
	defensesNotification: 0,
	missionsNotification: 0,
	documentsNotification: 0,

	// DOM elements
	snoop: undefined,

	init: function(_buildings, _actions, _defenses, _missions, _documents) {
		var self = this;
		// Game object initialisation

		this.attack = Zombies().init();

		$.each(_buildings, function(i, _building) {
			var newBuilding = Building(_building).init();
			self.buildings.push(newBuilding);
		});

		$.each(_actions, function(i, _action) {
			var newAction = Action(_action).init();
			self.actions.push(newAction);
		});

		$.each(_defenses, function(i, _defense) {
			var newDefense = Defense(_defense).init();
			self.defenses.push(newDefense);
		});

		$.each(_missions, function(i, _mission) {
			var newMission = Mission(_mission).init();
			self.missions.push(newMission);
		});

		$.each(_documents, function(i, _document) {
			var newDocument = Document(_document).init();
			self.documents.push(newDocument);
		});

		this.handle = window.setInterval(this._tick.bind(this), this.interval);
		this._checkAll();
		this._updateView();
	},

	_checkAll: function() {
		var self = this;

		this.technologiesNotification = 0;
		this.villageNotification = 0;
		this.defensesNotification = 0;
		this.missionsNotification = 0;
		this.documentsNotification = 0;

		$.each(this.buildings, function(i, building) {
			if (building.check())
				self.technologiesNotification++;
		});

		$.each(this.actions, function(i, action) {
			if (action.check())
				self.villageNotification++;
		});

		$.each(this.defenses, function(i, defense) {
			if (defense.check())
				self.defensesNotification++;
		});

		$.each(this.missions, function(i, mission) {
			if (mission.check())
				self.missionsNotification++;
		});

		$.each(this.documents, function(i, doc) {
			if (doc.check())
				self.documentsNotification++;
		});

		if (this.attack.check())
			this.villageNotification++;

		if (this.technologiesNotification > 0)
			$('.technologies-notification').text(this.technologiesNotification).show();
		else
			$('.technologies-notification').hide();

		if (this.villageNotification > 0)
			$('.village-notification').text(this.villageNotification).show();
		else
			$('.village-notification').hide();

		if (this.defensesNotification > 0)
			$('.defenses-notification').text(this.defensesNotification).show();
		else
			$('.defenses-notification').hide();

		if (this.missionsNotification > 0)
			$('.missions-notification').text(this.missionsNotification).show();
		else
			$('.missions-notification').hide();

		if (this.documentsNotification > 0)
			$('.documents-notification').text(this.documentsNotification).show();
		else
			$('.documents-notification').hide();
	},

	getResourceName: function(name) {
		if (name == 'scraps')
			return 'ferrailles';
		else if (name == 'energy')
			return 'énergie';
		else if (name == 'humans')
			return 'survivants';
		else if (name == 'defense')
			return 'défenses';
	},

	getResourceIcon: function(name) {
		if (name == 'scraps')
			return 'fa-cogs';
		else if (name == 'energy')
			return 'fa-bolt';
		else if (name == 'humans')
			return 'fa-user';
		else if (name == 'defense')
			return 'fa-shield-alt';
	},

	_tick: function() {
		// Called each tick, defined by this.interval
		$.each(this.buildings, function(i, building) {
			building.produce();
		});

		this._checkAll();

		this._updateView();
	},

	_updateView: function() {
		$('#humans').text(this.humans);
		$('#scraps').text(this.scraps);
		$('#energy').text(this.energy);
		$('#defenses').text(this.defense);
	}
};
