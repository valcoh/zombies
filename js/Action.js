var Action = function(options) {
	return $.extend(GameEntity(), {
		buy: function() {
			var self = this;

			$.each(this.cost, function(i, cost) {
				Game[cost.type] -= cost.value;
			});

			$.each(this.production, function(i, prod) {
				Game[prod.type] += prod.value;
			});

			Game._checkAll();
			Game._updateView();
		},

		init: function() {
			$('#actions-display').append(this.generateCard());

			this.check();

			return this;
		}
	},  options);
};
