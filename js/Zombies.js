var Zombies = function(options) {
	return $.extend({
		zombiesCount: 5,
		timestamp: undefined,
		chrono: undefined,
		display: undefined,
		seen: false,
		happened: false,
		increase: 1.1,

		check: function() {
			// On en profite pour updater le temps
			this.updateView();

			// On vérifie que le chrono n'est pas terminé
			if (Date.now() >= this.timestamp)
				this.endOfAttack();

			// Si une nouvelle attaque a eu lieu et n'a a pas été lue, on affiche une notification
			if (!this.seen && this.happened)
				return true;
			else
				return false;
		},

		updateView: function() {
			// On update le chronomètre dans le village
			var now = Date.now();

			var difference = new Date(this.timestamp - now);

			var res = {
				minutes: difference.getMinutes(),
				seconds: difference.getSeconds()
			};

			if (res.minutes < 10)
				res.minutes = '0' + res.minutes;
			if (res.seconds < 10)
				res.seconds = '0' + res.seconds;

			this.chrono.html('<b>Prochaine attaque dans : </b>' + res.minutes + ':' + res.seconds + '<br><b>Nombre de zombies : </b>' + this.zombiesCount);
		},

		endOfAttack: function() {
			// Appelée quand le compte à rebours arrive à zero
			// On calcule le résultat de l'attaque et on affiche la notification
			// On génère la prochaine attaque
			this.happened = true;

			var defenses = Game.defense,
					humans = Game.humans,
					energy = Game.energy,
					zombies = this.zombiesCount,
					lostEnergy = 0;

			// Un zombie meurt face à un point de défense
			if (zombies <= defenses) {
				// Epic win
				this.generateResult(true, 0, 0, zombies);
				return false;
			}
			else {
				// Les zombies meurent face aux défenses
				zombies -= defenses;
			}

			// Quand il n'y a plus de défenses, les survivants se battent
			// Se battre consomme de l'énergie, 10 par zombies
			var tmp = zombies * 10;

			if (tmp <= energy) {
				// Not that epic of a win
				this.generateResult(true, tmp, 0, zombies);
				return false;
			}
			else {
				// Les zombies meurent face aux survivants
				lostEnergy = Math.floor(energy / 10) * 10;
				zombies -= Math.floor(energy / 10);
			}

			// Une fois l'énergie vidée, un zombie meurt face à un survivant qui meurt aussi

			if (humans > zombies) {
				// Timid win
				this.generateResult(true, lostEnergy, humans - zombies, zombies);
				return false;
			}
			else {
				zombies -= humans;
			}

			// Echec, tous les survivants sont morts
			this.generateResult(false, lostEnergy, humans, zombies);
		},

		generateResult: function(won, lostEnergy, lostHumans, zombies) {
			Game.energy -= lostEnergy;
			Game.humans -= lostHumans;

			if (won) {
				this.display.text('Victoire ! Vous avez perdu ' + lostEnergy + ' énergie et ' + lostHumans + ' survivants.');
			}
			else {
				this.display.text('Vos défenses n\'ont pas suffit. Les ' + zombies + ' derniers zombis ont eu raison de vos survivants.');
			}
			this.init();
		},

		generateRandom: function(min, max) {
			var rand = Math.random() * (max - min) + min;

			return Math.round(rand * 10) / 10;
		},

		init: function() {
			// On génère le nombre de zombies de la prochaine attaque
			// On démarre le compte à rebours dans le village
			// Le nombre de zombies est légèrement randomisé et subis une increase comme les buildings
			this.zombiesCount *= this.increase;
			this.zombiesCount += this.zombiesCount * (this.generateRandom(-5, 5) / 100);
			this.zombiesCount = Math.round(this.zombiesCount);


			// On définit le timestamp de la prochaine attaque, 300000 correspond à 5min en millisecondes
			this.timestamp = Date.now() + 3000;

			// On définit l'élément du chrono
			this.chrono = $('#zombies-chrono');

			// On définit l'élément du display
			this.display = $('#attack-display');

			return this;
		}

	},  options);
};
