# Zombies Todo

## Priorité Haute

 - Plus d'interet à avoir des Survivants
 - Donner un ID aux documents pour les mettre en récompense des missions
 - Les zombies doivent dégrader l'état des défenses
 - Possibilité de réparer les défenses

## Priorité Faible

 - La gameloop doit se rafraichir plus souvent, tout en gardant les productions à la seconde (HARD)
 - Créer une interface pour éditer les informations du jeu
 - Créer un Objet pour déclencher des évènements
 - Système d'arbre des technologies pour acheter des upgrades
 - Système d'achievements
 - Faire apparaitre les onglets à partir d'un certain avancement
 - Nouveau thème pour le jeu
 - Passer en Vue.js
