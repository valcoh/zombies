<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Zombies</title>
	<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.min.css">
</head>
<body>
	<div class="container py-3">
		<div class="row mb-3">
			<div class="col-md">
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a class="navbar-brand" href="#">Zombies</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto" id="nav-buttons">
							<li class="village nav-item active">
								<a class="nav-link" href="#">Village <span class="badge badge-primary notification village-notification">0</span></a>
							</li>
							<li class="tech nav-item">
								<a class="nav-link" href="#">Technologies <span class="badge badge-primary notification technologies-notification">0</span></a>
							</li>
							<li class="defenses nav-item">
								<a class="nav-link" href="#">Défenses <span class="badge badge-primary notification defenses-notification">0</span></a>
							</li>
							<li class="missions nav-item">
								<a class="nav-link" href="#">Expéditions <span class="badge badge-primary notification missions-notification">0</span></a>
							</li>
							<li class="documents nav-item">
								<a class="nav-link" href="#">Documents <span class="badge badge-primary notification documents-notification">0</span></a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>

		<div class="row mb-3">
			<!-- Corps de la page -->
			<div class="col-md-8">
				<div class="row">

					<!-- Début du bloc village -->
					<div id="village-bloc" class="body-bloc">
						<div class="col-md-12">
							<h6>
								Attaques
							</h6>
						</div>
						<div class="col-md-12 mb-3">
							<div class="row">
								<div class="col-md-6">
									<div class="card">
										<div class="card-body">
											<div class="card-title" id="zombies-chrono">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="card">
										<div class="card-body">
											<div class="card-title" id="attack-display">

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<h6>
								Actions
							</h6>
						</div>
						<div class="col-md-12">
							<div class="row" id="actions-display">

							</div>
						</div>
					</div>
					<!-- Fin du bloc village -->

					<!-- Début du bloc technologies -->
					<div id="technologies-bloc" class="body-bloc" style="display: none;">
						<div class="col-md-12">
							<h6>
								Technologies
							</h6>
						</div>
						<div class="col-md-12">
							<div class="row" id="building-display">

							</div>
						</div>
					</div>
					<!-- Fin du bloc technologies -->

					<!-- Début du bloc défenses -->
					<div id="defenses-bloc" class="body-bloc" style="display: none;">
						<div class="col-md-12">
							<h6>
								Défenses
							</h6>
						</div>
						<div class="col-md-12">
							<div class="row" id="defenses-display">

							</div>
						</div>
					</div>
					<!-- Fin du bloc défenses -->

					<!-- Début du bloc missions -->
					<div id="missions-bloc" class="body-bloc" style="display: none;">
						<div class="col-md-12">
							<h6>
								Missions
							</h6>
						</div>
						<div class="col-md-12">
							<div class="alert alert-warning alert-dismissible fade show" role="alert">
							  Envoyer vos survivants en expédition est risqué. En cas d'échec, ils ne reviendront pas.
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							  </button>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row" id="missions-display">

							</div>
						</div>
					</div>
					<!-- Fin du bloc missions -->

					<!-- Début du bloc documents -->
					<div id="documents-bloc" class="body-bloc" style="display: none;">
						<div class="col-md-12">
							<h6>
								Documents
							</h6>
						</div>
						<div class="col-md-12" id="documents-display">
							<div class="row">
								<div class="col-md-12">
									<ul class="nav nav-tabs mb-3" id="documents-menu">

									</ul>
								</div>
								<div class="col-md-12" id="documents-content">

								</div>
							</div>
						</div>
					</div>
					<!-- Fin du bloc documents -->
				</div>
			</div>
			<!-- Fin du corps de la page -->

			<!-- Bloc de ressources à gauche -->
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-12">
						<h6>
							Ressources
						</h6>
					</div>

					<div class="col-md-12">
						<ul class="list-group">
							<li class="list-group-item d-flex justify-content-between align-items-center">
								<div class="wrap">
									<div class="icon">
										<i class="fa fa-shield-alt"></i>
									</div>
									Défenses
								</div>
								<div class="badge badge-primary badge-pill" id="defenses"></div>
							</li>
							<li class="list-group-item d-flex justify-content-between align-items-center">
								<div class="wrap">
									<div class="icon">
										<i class="fa fa-user"></i>
									</div>
									Survivants
								</div>
								<div class="badge badge-primary badge-pill" id="humans"></div>
							</li>
							<li class="list-group-item d-flex justify-content-between align-items-center">
								<div class="wrap">
									<div class="icon">
										<i class="fa fa-cogs"></i>
									</div>
									Ferraille
								</div>
								<div class="badge badge-primary badge-pill" id="scraps"></div>
							</li>
							<li class="list-group-item d-flex justify-content-between align-items-center">
								<div class="wrap">
									<div class="icon">
										<i class="fa fa-bolt"></i>
									</div>
									Energie
								</div>
								<div class="badge badge-primary badge-pill" id="energy"></div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- Fin du bloc de ressources -->

		</div>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
	<script src="js/GameEntity.js"></script>
	<script src="js/Zombies.js"></script>
	<script src="js/Building.js"></script>
	<script src="js/Action.js"></script>
	<script src="js/Defense.js"></script>
	<script src="js/Mission.js"></script>
	<script src="js/Document.js"></script>
	<script src="js/Game.js"></script>
	<script src="js/app.js"></script>
</body>
</html>
